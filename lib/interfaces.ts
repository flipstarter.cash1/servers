// Define the structure used to provide port numbers for supported transports.
export interface ElectrumTransports
{
	tcp?: number;
	tcp_tls?: number;
	ws?: number;
	wss?: number;
}

// Define the structure used to represent an electrum server.
export interface ElectrumServer
{
	host: string;
	version: string;
	transports: ElectrumTransports;
}

// Define the structure of the default electrum server list.
export type ElectrumServers = ElectrumServer[];
